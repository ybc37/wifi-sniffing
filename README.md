# Wi-Fi Sniffing

## Tasks

* Put Wi-Fi adapter in monitor mode
* Capture and explain a successful Wi-Fi authentication
* Capture and explain a failed Wi-Fi authentication (wrong password)

## Devices

* Wi-Fi access point (AP)
    * For simplicity, 2.4 GHz is disabled and the channel for 5 GHz is set manually
* Mobile device as client station (STA) to connect to AP
    * Some devices randomize the MAC address -> disable for easier filtering
* Notebook with Wi-Fi adapter (Intel Corporation Wireless 8265 / 8275)
    * Used in monitor mode to capture packages

## Preparation

Install `aircrack-ng` suite. It contains the `airmon-ng` script for switching to
monitor mode and back to managed mode. It depends on `iw`, which can also be
used to manage the mode and other wireless devices configuration.

```sh
sudo pacman -S aircrack-ng
```

Install Wireshark (CLI + GUI) for monitoring/filtering/tracing/...
(alternatively, `aircrack-ng` contains `airodump-ng`).

To use Wireshark as user, add the user to the `wireshark` group and reboot (or
make sure the changes are propagated somehow).

```sh
sudo pacman -S wireshark-{cli,qt}
sudo gpasswd -a <user> wireshark
```

## airmon-ng

`airmon-ng` is used to turn wireless cards into monitor mode.

```sh
# Show the interfaces status
sudo airmon-ng

# Check for potentially interfering programs
sudo airmon-ng check

# Kill potentially interfering programs
sudo airmon-ng check kill

# Start monitor mode
sudo airmon-ng start wlp59s0

# Start monitor mode with a specific channel (using `iwlist` to get currently used channel)
iwlist wlp59s0 channel
sudo airmon-ng start wlp59s0 <channel>

# In one line
sudo airmon-ng check kill && iwlist wlp59s0 channel | grep -Po '(?<=\(Channel )[0-9]{1-3}(?=\))' | xargs sudo airmon-ng start wlp59s0

# Stop monitor mode and return to managed mode (restart network service (`netctl`) for network)
sudo airmon-ng stop wlp59s0mon
```

## Wireshark

### Filters

Wireshark supports two types of filters: display and capture filters. Display
filters only act on the displayed result set; it still captures all packages.
To only capture specific packages, Wireshark supports capture filters using
PCAP.

Filters can be configured in the application (capture filters before starting
capturing, display filters while/after packets are captured).

Wireshark also provides a CLI (`tshark`).

#### Examples

##### Display Filters

Filter source and destination address:

```
wlan.sa == 01:23:45:67:89:AB or wlan.da == 01:23:45:67:89:AB
```

Filter probe requests and responses:

```
wlan.fc.type_subtype == 4 or wlan.fc.type_subtype == 5
```

Filter disassociate frames:

```
wlan.fc.type_subtype == 10
```

##### Capture Filters

Filter source and destination address:

```
wlan src 01:23:45:67:89:AB or wlan dst 01:23:45:67:89:AB
```

Filter `host` by MAC address of STA + `addr1` (= receiver; see [generic data
frame](https://www.oreilly.com/library/view/80211-wireless-networks/0596100523/ch04.html#wireless802dot112-CHP-4-SECT-1))
of STA and AP:

```
wlan host 01:23:45:67:89:AB or wlan addr1 01:23:45:67:89:AB or wlan addr1 01:23:45:67:89:AC
```

### CLI (tshark)

Capture filter:

```sh
tshark -f '<filter>'
```

Display filter:

```sh
tshark -Y '<filter>'
```

### Decrypt 802.11

See https://gitlab.com/wireshark/wireshark/-/wikis/HowToDecrypt802.11

## Task 1: Capture and Explain a Successful Wi-Fi Authentication

### 1. STA looks for AP by sending a Probe Request

It also includes supported data rates of the STA.

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Probe Request, Flags: ........C
    Type/Subtype: Probe Request (0x0004)
    Frame Control Field: 0x4000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Destination address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Transmitter address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Source address: MySTA_11:11:11 (98:9c:57:11:11:11)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    0000 0010 0100 .... = Sequence number: 36
    Frame check sequence: 0x8b50e771 [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Tagged parameters (86 bytes)
        Tag: SSID parameter set: FRITZ!Repeater 2400
        Tag: Supported Rates 6(B), 9, 12(B), 18, 24(B), 36, 48, 54, [Mbit/sec]
        Tag: DS Parameter set: Current Channel: 100
        Tag: HT Capabilities (802.11n D1.10)
        Tag: Extended Capabilities (8 octets)
        Tag: VHT Capabilities
```

</details>

### 2. AP sends Probe Response to STA (if supported data rates are compatible)

The response also includes the encryption type.

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Probe Response, Flags: ........C
    Type/Subtype: Probe Response (0x0005)
    Frame Control Field: 0x5000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    1110 0101 1111 .... = Sequence number: 3679
    Frame check sequence: 0x63dbe6b3 [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Fixed parameters (12 bytes)
    Tagged parameters (456 bytes)
        Tag: SSID parameter set: FRITZ!Repeater 2400
        Tag: Supported Rates 6(B), 9, 12(B), 18, 24(B), 36, 48, 54, [Mbit/sec]
        Tag: DS Parameter set: Current Channel: 100
        Tag: Country Information: Country Code DE, Environment Any
        Tag: Power Constraint: 3
        Tag: QBSS Load Element 802.11e CCA Version
        Tag: RM Enabled Capabilities (5 octets)
        Tag: HT Capabilities (802.11n D1.10)
        Tag: HT Information (802.11n D1.10)
        Tag: Overlapping BSS Scan Parameters
        Tag: Extended Capabilities (8 octets)
        Tag: VHT Capabilities
        Tag: VHT Operation
        Tag: Extended BSS Load
        Tag: VHT Tx Power Envelope
        Tag: Vendor Specific: Microsoft Corp.: WMM/WME: Parameter Element
        Tag: Vendor Specific: Atheros Communications, Inc.: Advanced Capability
        Tag: Vendor Specific: AVM GmbH
        Tag: Vendor Specific: Atheros Communications, Inc.: Unknown
        Tag: Vendor Specific: Qualcomm Inc.
        Tag: Vendor Specific: Microsoft Corp.: WPS
        Tag: RSN Information
        Tag: Vendor Specific: Qualcomm Inc.
```

</details>

### 3. STA sends an Authentication Request to AP (auth seq = 0x0001)

Authentication algorithm is set to *open*, because authentication frames were
designed for WEP, which is not secure anymore. Other authentication methods
(e.g. WPA2) will take place after authentication and association.

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Authentication, Flags: ........C
    Type/Subtype: Authentication (0x000b)
    Frame Control Field: 0xb000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Destination address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Transmitter address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Source address: MySTA_11:11:11 (98:9c:57:11:11:11)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    0000 0000 0000 .... = Sequence number: 0
    Frame check sequence: 0x1da6cc36 [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Fixed parameters (6 bytes)
        Authentication Algorithm: Open System (0)
        Authentication SEQ: 0x0001
        Status code: Successful (0x0000)
```

</details>

### 4. AP responds with an Authentication Response (auth seq = 0x0002)

A response with sequence 0x0002 indicates a successful authentication.

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Authentication, Flags: ........C
    Type/Subtype: Authentication (0x000b)
    Frame Control Field: 0xb000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    1110 0110 0000 .... = Sequence number: 3680
    Frame check sequence: 0x683797a0 [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Fixed parameters (6 bytes)
        Authentication Algorithm: Open System (0)
        Authentication SEQ: 0x0002
        Status code: Successful (0x0000)
```

</details>

### 5. STA sends an Association Request to AP

This request also contains the information about the encryption type (tag RSN =
WPA2 = 802.11i)

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Association Request, Flags: ........C
    Type/Subtype: Association Request (0x0000)
    Frame Control Field: 0x0000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Destination address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Transmitter address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Source address: MySTA_11:11:11 (98:9c:57:11:11:11)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    0000 0000 0001 .... = Sequence number: 1
    Frame check sequence: 0x3949c0d4 [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Fixed parameters (4 bytes)
        Capabilities Information: 0x1511
        Listen Interval: 0x0003
    Tagged parameters (150 bytes)
        Tag: SSID parameter set: FRITZ!Repeater 2400
        Tag: Supported Rates 6(B), 9, 12(B), 18, 24(B), 36, 48, 54, [Mbit/sec]
        Tag: Power Capability Min: 3, Max: 23
        Tag: Supported Channels
        Tag: Vendor Specific: Microsoft Corp.: WMM/WME: Information Element
        Tag: HT Capabilities (802.11n D1.10)
        Tag: Extended Capabilities (8 octets)
        Tag: VHT Capabilities
        Tag: Vendor Specific: Huawei Technologies Co.,Ltd
        Tag: RSN Information
            Tag Number: RSN Information (48)
            Tag length: 20
            RSN Version: 1
            Group Cipher Suite: 00:0f:ac (Ieee 802.11) AES (CCM)
            Pairwise Cipher Suite Count: 1
            Pairwise Cipher Suite List 00:0f:ac (Ieee 802.11) AES (CCM)
            Auth Key Management (AKM) Suite Count: 1
            Auth Key Management (AKM) List 00:0f:ac (Ieee 802.11) PSK
            RSN Capabilities: 0x0000
        Tag: Supported Operating Classes
```

</details>

### 6. AP responds with an Association Response

`Status code` indicates if the association was successful.

<details>
<summary>Packet content</summary>

```
IEEE 802.11 Association Response, Flags: ........C
    Type/Subtype: Association Response (0x0001)
    Frame Control Field: 0x1000
    .000 0000 0011 1100 = Duration: 60 microseconds
    Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
    Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
    BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
    .... .... .... 0000 = Fragment number: 0
    1110 0110 0001 .... = Sequence number: 3681
    Frame check sequence: 0xc2c4703b [unverified]
    [FCS Status: Unverified]
IEEE 802.11 Wireless Management
    Fixed parameters (6 bytes)
        Capabilities Information: 0x1511
        Status code: Successful (0x0000)
        ..00 0000 0000 0010 = Association ID: 0x0002
    Tagged parameters (187 bytes)
        Tag: Supported Rates 6(B), 9, 12(B), 18, 24(B), 36, 48, 54, [Mbit/sec]
        Tag: RM Enabled Capabilities (5 octets)
        Tag: HT Capabilities (802.11n D1.10)
        Tag: HT Information (802.11n D1.10)
        Tag: Overlapping BSS Scan Parameters
        Tag: Extended Capabilities (8 octets)
        Tag: VHT Capabilities
        Tag: VHT Operation
        Tag: Vendor Specific: Microsoft Corp.: WMM/WME: Parameter Element
        Tag: Vendor Specific: Qualcomm Inc.
        Tag: Vendor Specific: Microsoft Corp.: WPS
        Tag: Vendor Specific: Atheros Communications, Inc.: Unknown
```

</details>

### 7. 802.11i four-way handshake

If no further authentication (WPA/WPA2,...) is used, data can be transferred,
now.

All messages for the four-way handshakes are sent as EAPOL frames.

1. AP sends Anonce to STA, so that STA can generate PTK
    <details>
    <summary>Packet content</summary>

    ```
    IEEE 802.11 QoS Data, Flags: ......F.C
        Type/Subtype: QoS Data (0x0028)
        Frame Control Field: 0x8802
        .000 0000 0011 1100 = Duration: 60 microseconds
        Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
        STA address: MySTA_11:11:11 (98:9c:57:11:11:11)
        .... .... .... 0000 = Fragment number: 0
        0000 0000 0000 .... = Sequence number: 0
        Frame check sequence: 0xab40be2d [unverified]
        [FCS Status: Unverified]
        Qos Control: 0x0006
    802.1X Authentication
        Version: 802.1X-2004 (2)
        Type: Key (3)
        Length: 95
        Key Descriptor Type: EAPOL RSN Key (2)
        [Message number: 1]
        Key Information: 0x008a
        Key Length: 16
        Replay Counter: 1
        WPA Key Nonce: 4c0dbe85a0e8ed947caa0add47bb07412e39d2fe13d38cfd59a18e18eb10fdb9
        Key IV: 00000000000000000000000000000000
        WPA Key RSC: 0000000000000000
        WPA Key ID: 0000000000000000
        WPA Key MIC: 00000000000000000000000000000000
        WPA Key Data Length: 0
    ```

    </details>

2. STA sends Snonce with MIC to AP, so that AP can also generates PTK
    <details>
    <summary>Packet content</summary>

    ```
    IEEE 802.11 QoS Data, Flags: .......TC
        Type/Subtype: QoS Data (0x0028)
        Frame Control Field: 0x8801
        .000 0000 0011 1100 = Duration: 60 microseconds
        Receiver address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Transmitter address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Destination address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Source address: MySTA_11:11:11 (98:9c:57:11:11:11)
        BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
        STA address: MySTA_11:11:11 (98:9c:57:11:11:11)
        .... .... .... 0000 = Fragment number: 0
        0000 0000 0000 .... = Sequence number: 0
        Frame check sequence: 0xf68b44ca [unverified]
        [FCS Status: Unverified]
        Qos Control: 0x0007
    802.1X Authentication
        Version: 802.1X-2001 (1)
        Type: Key (3)
        Length: 117
        Key Descriptor Type: EAPOL RSN Key (2)
        [Message number: 2]
        Key Information: 0x010a
        Key Length: 0
        Replay Counter: 1
        WPA Key Nonce: f8d56b1c1832d059a8e187b2966ebae5771d44f98b0c2015258c20ab42b3dba4
        Key IV: 00000000000000000000000000000000
        WPA Key RSC: 0000000000000000
        WPA Key ID: 0000000000000000
        WPA Key MIC: bab0a9b62bfe981435d08e8a878ea585
        WPA Key Data Length: 22
        WPA Key Data: 30140100000fac040100000fac040100000fac020000
    ```

    </details>

3. AP verifies message from STA and if valid, constructs and sends GTK and also
   a MIC
    <details>
    <summary>Packet content</summary>

    ```
    IEEE 802.11 QoS Data, Flags: ......F.C
        Type/Subtype: QoS Data (0x0028)
        Frame Control Field: 0x8802
        .000 0000 0011 1100 = Duration: 60 microseconds
        Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
        STA address: MySTA_11:11:11 (98:9c:57:11:11:11)
        .... .... .... 0000 = Fragment number: 0
        0000 0000 0001 .... = Sequence number: 1
        Frame check sequence: 0xe077e3b8 [unverified]
        [FCS Status: Unverified]
        Qos Control: 0x0006
    802.1X Authentication
        Version: 802.1X-2004 (2)
        Type: Key (3)
        Length: 151
        Key Descriptor Type: EAPOL RSN Key (2)
        [Message number: 3]
        Key Information: 0x13ca
        Key Length: 16
        Replay Counter: 2
        WPA Key Nonce: 4c0dbe85a0e8ed947caa0add47bb07412e39d2fe13d38cfd59a18e18eb10fdb9
        Key IV: 00000000000000000000000000000000
        WPA Key RSC: 0000000000000000
        WPA Key ID: 0000000000000000
        WPA Key MIC: 4646d43e3810592e5dad2fba1d2d1ce6
        WPA Key Data Length: 56
        WPA Key Data: e3e14b6f8a34f038c5f543e77e58b9178d8c622ae1d226836760e83b34889ee20554807d…
    ```

    </details>

4. STA verifies message from AP and if valid, sends a confirmation to AP
    <details>
    <summary>Packet content</summary>

    ```
    IEEE 802.11 QoS Data, Flags: .......TC
        Type/Subtype: QoS Data (0x0028)
        Frame Control Field: 0x8801
        .000 0000 0011 1100 = Duration: 60 microseconds
        Receiver address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Transmitter address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Destination address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Source address: MySTA_11:11:11 (98:9c:57:11:11:11)
        BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
        STA address: MySTA_11:11:11 (98:9c:57:11:11:11)
        .... .... .... 0000 = Fragment number: 0
        0000 0000 0001 .... = Sequence number: 1
        Frame check sequence: 0x9694243b [unverified]
        [FCS Status: Unverified]
        Qos Control: 0x0007
    802.1X Authentication
        Version: 802.1X-2001 (1)
        Type: Key (3)
        Length: 95
        Key Descriptor Type: EAPOL RSN Key (2)
        [Message number: 4]
        Key Information: 0x030a
        Key Length: 0
        Replay Counter: 2
        WPA Key Nonce: 0000000000000000000000000000000000000000000000000000000000000000
        Key IV: 00000000000000000000000000000000
        WPA Key RSC: 0000000000000000
        WPA Key ID: 0000000000000000
        WPA Key MIC: c9998ef5febd074506e979ebe637b267
        WPA Key Data Length: 0
    ```

    </details>

#### Definitions

* Anonce: random number generated by AP
* Snonce: random number generated by STA
* MIC: Message Integrity Code
    * like a signature to confirm identity of sender
* PTK: Pairwise Transit Key
    * unique between AP and STA
    * used to encrypt traffic
    * PTK = PRF (PMK + Anonce + SNonce + Mac (STA) + Mac (AP))
* PRF: Pseudo-Random Function
    * applied to input of PTK
* PMK: Pairwise Master Key
    * same as PSK when using WPA/WPA2
* PSK: Pre-Shared Key
    * derived from WPA/WPA2 password
* GTK: Group Temporal Key
    * shared between AP and all connected STAs
    * used to encrypt all broadcast and multicast messages
    * derived from GMK
* GMK: Group Master Key
    * used to create GTK

## Task 2: Capture and Explain a Failed Wi-Fi Authentication (Wrong Password)

Even with a wrong password, the 802.11 association process is successful. At
least with WPA/WPA2; only with WEP the actual authentication is done with the
Authentication Requests/Respnses.

This means probing, authentication and association is successful as in task 1
(1. - 6.).

### Failing 802.11i four-way handshake

The four-way handshakes fails after four failed attemps.

1. AP sends Anonce to STA, so that STA can generate PTK
2. STA sends Snonce with MIC to AP, so that AP can also generates PTK
3. AP fails to verifies message from STA
4. AP sends again (the same) Anonce (see 1.)
    * `Message Number` is reset to 1
    * `Replay Counter` is incremented to 2
5. STA sends same Sonce as before (see 2.) with MIC
6. 3rd attempt (see 3. and 4.): `Message Number` = 1, `Replay Counter` = 3
7. 4th attempt (see 3. and 4.): `Message Number` = 1, `Replay Counter` = 4
8. AP sends Disassociate frame
    <details>
    <summary>Packet content</summary>

    ```
    IEEE 802.11 Disassociate, Flags: ........C
        Type/Subtype: Disassociate (0x000a)
        Frame Control Field: 0xa000
        .000 0000 0011 1100 = Duration: 60 microseconds
        Receiver address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Destination address: MySTA_11:11:11 (98:9c:57:11:11:11)
        Transmitter address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        Source address: MyAP_00:00:00 (2c:91:ab:00:00:00)
        BSS Id: MyAP_00:00:00 (2c:91:ab:00:00:00)
        .... .... .... 0000 = Fragment number: 0
        0000 0000 0000 .... = Sequence number: 0
        Frame check sequence: 0x30a7c2c7 [unverified]
        [FCS Status: Unverified]
    IEEE 802.11 Wireless Management
        Fixed parameters (2 bytes)
            Reason code: Previous authentication no longer valid (0x0002)
    ```

    </details>

## Resources

* [airmon-ng](https://aircrack-ng.org/) ([repo](https://github.com/aircrack-ng/aircrack-ng))
    * https://aircrack-ng.org/doku.php?id=airmon-ng
    * https://aircrack-ng.org/doku.php?id=airodump-ng
* [Wireshark](https://www.wireshark.org/) ([repo](https://gitlab.com/wireshark/wireshark))
    * https://wiki.archlinux.org/index.php/Wireshark
    * https://wiki.wireshark.org/CaptureSetup/WLAN
    * https://www.semfionetworks.com/uploads/2/9/8/3/29831147/wireshark_802.11_filters_-_reference_sheet.pdf
* 802.11
    * https://wifibond.com/2017/04/08/802-11-association-process/
    * https://packet6.com/802-11-state-machine/
    * https://www.dinotools.de/2017/03/24/ieee-802-11-grundlagen-und-verbindungsaufbau/
    * https://documentation.meraki.com/MR/WiFi_Basics_and_Best_Practices/802.11_Association_Process_Explained
    * https://www.oreilly.com/library/view/80211-wireless-networks/0596100523/ch04.html
    * https://howiwifi.com/2020/07/13/802-11-frame-types-and-formats/
* 802.11i
    * https://en.wikipedia.org/wiki/IEEE_802.11i-2004
    * https://www.wifi-professionals.com/2019/01/4-way-handshake
    * https://medium.com/@alonr110/the-4-way-handshake-wpa-wpa2-encryption-protocol-65779a315a64
